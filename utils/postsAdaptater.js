export const postAdaptater = ({
    slug,
    title,
    category,
    seo,
    locale,
    author,
}) => {
    return {
        slug,
        title,
        category,
        seo,
        locale,
        author,
    }
}

export const postsAdaptater = (posts) => {
    return posts.map((post) => postAdaptater({ ...post }))
}
