// Decide what the button will look like based on its type (primary or secondary)
// and on its background (light or dark).
export function getButtonAppearance(type, background) {
    if (type === "primary") {
        if (background === "light") {
            // Dark primary button on a light background
            return "primary"
        }
        // Fully white primary button on a dark background
        return "primary"
    }

    if (type === "secondary") {
        if (background === "light") {
            // Dark outline primary button on a light background
            return "secondary"
        }
        // White outline primary button on a dark background
        return "secondary"
    }

    if (type === "white") {
        if (background === "light") {
            // Dark outline primary button on a light background
            return "white"
        }
        // White outline primary button on a dark background
        return "white"
    }

    if (type === "black") {
        if (background === "light") {
            // Dark outline primary button on a light background
            return "black"
        }
        // White outline primary button on a dark background
        return "black"
    }
    // Shouldn't happen, but default to dark button just in case
    return "white"
}
