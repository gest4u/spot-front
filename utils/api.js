export function getStrapiURL(path) {
    return `${
        process.env.NEXT_PUBLIC_STRAPI_API_URL || "http://localhost:1337"
    }${path}`
}

// Helper to make GET requests to Strapi
export async function fetchAPI(path, options = {}) {
    const defaultOptions = {
        headers: {
            "Content-Type": "application/json",
        },
    }
    const mergedOptions = {
        ...defaultOptions,
        ...options,
    }
    const requestUrl = getStrapiURL(path)
    const response = await fetch(requestUrl, mergedOptions)

    if (!response.ok) {
        console.error(response)
        throw new Error(`An error occured please try again`)
    }
    const data = await response.json()
    return data
}

/**
 * GetPageData
 *
 * @param {object} params The router params object with slug: { slug: [<slug>] }
 * @param {string} locale The current locale specified in router.locale
 * @param {boolean} preview router isPreview value
 */
export async function getPageData(params, locale, preview) {
    const slug = params.slug.join("/")
    // Find the pages that match this slug
    const pagesData = await fetchAPI(
        `/pages?slug=${slug}&_locale=${locale}&_publicationState=${
            preview ? "preview" : "live"
        }`
    )
    // Make sure we found something, otherwise return null
    if (pagesData == null || pagesData.length === 0) {
        return null
    }
    // Return the first item since there should only be one result per slug
    return pagesData[0]
}

/**
 * GetAllPostsData
 *
 * @param {string} locale The current locale specified in router.locale
 * @param {boolean} preview router isPreview value
 */
export async function getAllPostsData(locale, preview) {
    // Find the pages that match this slug
    const allPostsData = await fetchAPI(
        `/posts/?_locale=${locale}&_publicationState=${
            preview ? "preview" : "live"
        }`
    )
    // Return the first item since there should only be one result per slug
    return allPostsData
}

/**
 * GetHighlightedPostsData
 *
 * @param {string} locale The current locale specified in router.locale
 * @param {boolean} preview router isPreview value
 */
export async function getHighlightedPostsData(locale, preview) {
    // Find the pages that match this slug
    const highlightedPostsData = await fetchAPI(
        `/posts/?_locale=${locale}&_start=0&_limit=3&_publicationState=${
            preview ? "preview" : "live"
        }`
    )
    // Return the first item since there should only be one result per slug
    return highlightedPostsData
}

/**
 * GetPostData
 *
 * @param {object} params The router params object with slug: { slug: [<slug>] }
 * @param {string} locale The current locale specified in router.locale
 * @param {boolean} preview router isPreview value
 */
export async function getPostData(params, locale, preview) {
    const slug = params.slug.join("/")
    // Find the pages that match this slug
    const postData = await fetchAPI(
        `/posts/?_locale=${locale}&_publicationState=${
            preview ? "preview" : "live"
        }`
    )
    // Make sure we found something, otherwise return null
    if (postData == null || postData.length === 0) {
        return null
    }
    // Return the first item since there should only be one result per slug
    return postData[0]
}

// Get site data from Strapi (metadata, navbar, footer...)
export async function getGlobalData(locale) {
    const global = await fetchAPI(`/global?_locale=${locale}`)
    return global
}
