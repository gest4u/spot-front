import { fetchAPI } from "./api"

/**
 * getLocalizedPage
 *
 * @param {string} viewContext build th post paths
 *  @param {string} targetLocale build th post paths
 */
export async function getLocalizedView(targetLocale, viewContext) {
    const localization = viewContext.localizations.find(
        (localization) => localization.locale === targetLocale
    )
    const localeView = await fetchAPI(`/pages/${localization.id}`)
    return localeView
}

/**
 * localizePath
 *
 * @param {string} page build th post paths
 */
export function localizePath(page) {
    const { locale, defaultLocale, slug } = page
    if (locale === defaultLocale) {
        // The default locale is not prefixed
        return `/${slug}`
    }
    // The slug should have a localePrefix
    return `/${locale}/${slug}`
}

/**
 * getLocalizedPaths
 *
 */
export function getLocalizedPaths(page) {
    const paths = page.locales.map((locale) => {
        return {
            locale: locale,
            href: localizePath({ ...page, locale }),
        }
    })
    return paths
}

/**
 * localizePostPath
 *
 * @param {string} page build th post paths
 */
export function localizePostPath(page) {
    const { locale, defaultLocale, slug } = page
    if (locale === defaultLocale) {
        // The default locale is not prefixed
        return `/blog/article/${slug}`
    }
    // The slug should have a localePrefix
    return `/blog/article/${locale}/${slug}`
}

/**
 * getLocalizedPostPaths
 *
 * @param {string} page The current locale specified in router.locale
 */
export function getLocalizedPostPaths(page) {
    const paths = page.locales.map((locale) => {
        return {
            locale: locale,
            href: localizePostPath({ ...page, locale }),
        }
    })
    return paths
}
