import { InView } from "react-intersection-observer"
import Zoom from "react-medium-image-zoom"
import "react-medium-image-zoom/dist/styles.css"
import NextImage from "@/components/elements/image"
import classNames from "classnames"

const Gallery = ({ data }) => {
    return (
        <section className=" flex flex-col md:flex-row items-center justify-between py-12">
            {data.images.map((image, index) => (
                <div key={index} className="relative flex flex-col w-1/2">
                    <div className="w-1/2 h-48 bg-ember-100 z-10">
                        <InView threshold="0.2" triggerOnce="true">
                            {({ inView, ref }) => (
                                <div ref={ref} className="w-full h-full">
                                    <Zoom zoomMargin={50}>
                                        <NextImage
                                            media={image.media}
                                            cover
                                            className={classNames(
                                                "w-full h-full object-center object-cover",
                                                inView
                                                    ? "is-in-view"
                                                    : "is-out-view"
                                            )}
                                            data-anim="fade"
                                        />
                                    </Zoom>
                                </div>
                            )}
                        </InView>
                    </div>
                    <div className="w-1/2 h-48 self-end bg-ember-100 z-10">
                        <InView threshold="0.2" triggerOnce="true">
                            {({ inView, ref }) => (
                                <div ref={ref} className="w-full h-full">
                                    <Zoom zoomMargin={50}>
                                        <NextImage
                                            media={image.media}
                                            cover
                                            className={classNames(
                                                "w-full h-full object-center object-cover",
                                                inView
                                                    ? "is-in-view"
                                                    : "is-out-view"
                                            )}
                                            data-anim="fade"
                                        />
                                    </Zoom>
                                </div>
                            )}
                        </InView>
                    </div>
                    {/*décoration*/}
                    <div className="bg-amber-100 w-full h-48 absolute bottom-0 right-full z-0"></div>
                </div>
            ))}
        </section>
    )
}

export default Gallery
