import Link from "next/link"

const highlightedPostsList = ({ data, highlightedPostsData }) => {
    return (
        <div className="my-16 py-16 bg-gray-800 overflow-hidden">
            <div className="relative container px-4">
                <div className="relative">
                    {data.title && (
                        <h1 className="text-4xl text-center font-extrabold tracking-tight mb-8 text-white">
                            {data.title}
                        </h1>
                    )}
                    {highlightedPostsData && (
                        <ul className="max-w-5xl m-auto divide-y divide-gray-700">
                            {highlightedPostsData.map((highlightedPotsData) => (
                                <li
                                    className="my-2"
                                    key={highlightedPotsData.id}
                                >
                                    <Link
                                        as={`/blog/article/${highlightedPotsData.slug}`}
                                        href="blog/article/[id]"
                                    >
                                        <a className="relative flex items-center justify-between space-x-4 mt-4">
                                            <div className="flex items-center space-x-4">
                                                {/*<div className="flex-shrink-0">
                                                    <div className="flex items-center justify-center h-12 w-12 rounded-md bg-white text-gray-800">
                                                        1
                                                    </div>
                                                </div>*/}
                                                <div className="flex-1 min-w-0">
                                                    <div className="relative focus-within:ring-2 focus-within:ring-indigo-500">
                                                        <h3 className="text-sm font-semibold text-white">
                                                            <span
                                                                href="#"
                                                                className="hover:underline focus:outline-none"
                                                            >
                                                                {
                                                                    highlightedPotsData.shortName
                                                                }
                                                            </span>
                                                        </h3>
                                                        {highlightedPotsData.excerpt && (
                                                            <p className="mt-1 text-sm text-gray-400 line-clamp-2">
                                                                {
                                                                    highlightedPotsData.excerpt
                                                                }
                                                            </p>
                                                        )}
                                                    </div>
                                                </div>
                                            </div>
                                            <span aria-hidden="true">
                                                <svg
                                                    className="h-5 w-5 text-gray-500"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 0 20 20"
                                                    fill="currentColor"
                                                    aria-hidden="true"
                                                >
                                                    <path
                                                        fillRule="evenodd"
                                                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                                        clipRule="evenodd"
                                                    ></path>
                                                </svg>
                                            </span>
                                        </a>
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    )}
                </div>
            </div>
        </div>
    )
}
export default highlightedPostsList
