import classNames from "classnames"
import { InView } from "react-intersection-observer"
import Zoom from "react-medium-image-zoom"
import NextImage from "@/components/elements/image"

const Overview = ({ data }) => {
    return (
        <section
            id="overview"
            className="overflow-hidden bg-gray-100 flex flex-col md:flex-row items-center justify-between pt-12 lg:pt-24"
        >
            <div className="relative container mx-auto max-w-7xl px-4">
                {/* svg decoration */}
                <svg
                    className="hidden lg:block absolute bottom-0 left-full transform -translate-x-1/2 "
                    width={404}
                    height={784}
                    fill="none"
                    viewBox="0 0 404 784"
                    aria-hidden="true"
                >
                    <defs>
                        <pattern
                            id="b1e6e422-73f8-40a6-b5d9-c8586e37e0e7"
                            x={0}
                            y={0}
                            width={20}
                            height={20}
                            patternUnits="userSpaceOnUse"
                        >
                            <rect
                                x={0}
                                y={0}
                                width={4}
                                height={4}
                                className="text-gray-200"
                                fill="currentColor"
                            />
                        </pattern>
                    </defs>
                    <rect
                        width={404}
                        height={784}
                        fill="url(#b1e6e422-73f8-40a6-b5d9-c8586e37e0e7)"
                    />
                </svg>
                <svg
                    className="hidden lg:block absolute bottom-0 right-full transform translate-x-1/2 "
                    width={404}
                    height={784}
                    fill="none"
                    viewBox="0 0 404 784"
                    aria-hidden="true"
                >
                    <defs>
                        <pattern
                            id="b1e6e422-73f8-40a6-b5d9-c8586e37e0e7"
                            x={0}
                            y={0}
                            width={20}
                            height={20}
                            patternUnits="userSpaceOnUse"
                        >
                            <rect
                                x={0}
                                y={0}
                                width={4}
                                height={4}
                                className="text-gray-200"
                                fill="currentColor"
                            />
                        </pattern>
                    </defs>
                    <rect
                        width={404}
                        height={784}
                        fill="url(#b1e6e422-73f8-40a6-b5d9-c8586e37e0e7)"
                    />
                </svg>
                {/* intro */}
                <div className="relative pb-24">
                    <InView threshold="0.2" triggerOnce="true">
                        {({ inView, ref }) => (
                            <div
                                ref={ref}
                                className="overflow-hidden leading-5"
                            >
                                <h2
                                    className={classNames(
                                        inView ? "is-in-view" : "is-out-view",
                                        "text-center text-5xl  sm:text-6xl leading-8 font-extrabold tracking-tight text-gray-800"
                                    )}
                                    data-anim="slide"
                                >
                                    {data.title}
                                </h2>
                            </div>
                        )}
                    </InView>
                    <p className="mt-4 max-w-3xl mx-auto text-center text-xl text-gray-500">
                        {data.desc}
                    </p>
                </div>
                <div className="relative lg:grid lg:grid-cols-2 lg:gap-0 lg:items-start ">
                    {/* part */}
                    <div className="relative py-4 lg:py-0 border-t lg:border-r lg:border-t-0 border-gray-500 border-dashed lg:h-full bg-gradient-to-b from-white to-gray-100">
                        <div className="p-10 mt-5">
                            <h3 className="lg:text-center text-2xl font-extrabold text-gray-800 tracking-tight sm:text-3xl">
                                {data.spaces.title}
                            </h3>
                            <p className="lg:text-center mt-3 text-lg text-gray-500">
                                {data.spaces.description}
                            </p>
                            <ul className="mt-10 space-y-10">
                                {data.spaces.list.map((listItem, index) => (
                                    <li key={index} className="relative">
                                        <div className="absolute flex items-center justify-center h-12 w-12 rounded-md bg-amber-100 dark">
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 20 20"
                                                fill="currentColor"
                                                className="flex-shrink-0 h-5 w-5 text-gray-800"
                                                aria-hidden="true"
                                            >
                                                <path
                                                    fillRule="evenodd"
                                                    d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                                                    clipRule="evenodd"
                                                ></path>
                                            </svg>
                                        </div>
                                        <div className="ml-16 ">
                                            <h3 className="text-lg g-3 font-medium text-gray-800">
                                                {listItem.title}
                                            </h3>
                                            <p className="text-gray-500 text-sm">
                                                {listItem.description}
                                            </p>
                                        </div>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        <div className="relative flex flex-col">
                            <div className="w-1/2 h-48 bg-ember-100">
                                <InView threshold="0.2" triggerOnce="true">
                                    {({ inView, ref }) => (
                                        <div
                                            ref={ref}
                                            className={classNames(
                                                inView
                                                    ? "is-in-view"
                                                    : "is-out-view",
                                                "w-full h-full"
                                            )}
                                        >
                                            <Zoom zoomMargin={50}>
                                                <NextImage
                                                    media={
                                                        data.spaces.images[0]
                                                            .media
                                                    }
                                                    cover
                                                    className="w-full h-full object-center object-cover"
                                                    data-anim="fade"
                                                />
                                            </Zoom>
                                        </div>
                                    )}
                                </InView>
                            </div>
                            <div className="w-1/2 h-48 self-end bg-ember-100">
                                <InView threshold="0.2" triggerOnce="true">
                                    {({ inView, ref }) => (
                                        <div
                                            ref={ref}
                                            className={classNames(
                                                inView
                                                    ? "is-in-view"
                                                    : "is-out-view",
                                                "w-full h-full"
                                            )}
                                        >
                                            <Zoom zoomMargin={50}>
                                                <NextImage
                                                    media={
                                                        data.spaces.images[1]
                                                            .media
                                                    }
                                                    cover
                                                    className="w-full h-full object-center object-cover"
                                                    data-anim="fade"
                                                />
                                            </Zoom>
                                        </div>
                                    )}
                                </InView>
                            </div>
                            {/*décoration*/}
                            <div className="bg-amber-100 w-full h-48 absolute bottom-0 right-full z-0"></div>
                        </div>
                    </div>
                    {/* part */}
                    <div className="relative py-4 lg:py-0 border-t lg:border-t-0 border-gray-500 border-dashed bg-gradient-to-b from-white to-gray-100">
                        <div className="relative p-10 mt-5">
                            <h3 className="lg:text-center text-2xl font-extrabold text-gray-800 tracking-tight sm:text-3xl">
                                {data.rooms.title}
                            </h3>
                            <p className="lg:text-center mt-3 text-lg text-gray-500">
                                {data.rooms.description}
                            </p>
                            <ul className="mt-10 space-y-10">
                                {data.rooms.list.map((listItem, index) => (
                                    <li key={index} className="relative">
                                        <div className="absolute flex items-center justify-center h-12 w-12 rounded-md bg-amber-100 dark">
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 20 20"
                                                fill="currentColor"
                                                className="flex-shrink-0 h-5 w-5 text-gray-800"
                                                aria-hidden="true"
                                            >
                                                <path
                                                    fillRule="evenodd"
                                                    d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                                                    clipRule="evenodd"
                                                ></path>
                                            </svg>
                                        </div>
                                        <div className="ml-16">
                                            <h3 className="text-lg font-medium text-gray-800">
                                                {listItem.title}
                                            </h3>
                                            <p className="text-gray-500 text-sm">
                                                {listItem.description}
                                            </p>
                                        </div>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        <div className="relative flex flex-col">
                            <div className="w-1/2 h-48 bg-ember-100">
                                <InView threshold="0.2" triggerOnce="true">
                                    {({ inView, ref }) => (
                                        <div
                                            ref={ref}
                                            className={classNames(
                                                inView
                                                    ? "is-in-view"
                                                    : "is-out-view",
                                                "w-full h-full"
                                            )}
                                        >
                                            <Zoom zoomMargin={50}>
                                                <NextImage
                                                    media={
                                                        data.rooms.images[0]
                                                            .media
                                                    }
                                                    cover
                                                    className="w-full h-full object-center object-cover"
                                                    data-anim="fade"
                                                />
                                            </Zoom>
                                        </div>
                                    )}
                                </InView>
                            </div>
                            <div className="w-1/2 h-48 self-end bg-ember-100">
                                <InView threshold="0.2" triggerOnce="true">
                                    {({ inView, ref }) => (
                                        <div
                                            ref={ref}
                                            className={classNames(
                                                inView
                                                    ? "is-in-view"
                                                    : "is-out-view",
                                                "w-full h-full"
                                            )}
                                        >
                                            <Zoom zoomMargin={50}>
                                                <NextImage
                                                    media={
                                                        data.rooms.images[1]
                                                            .media
                                                    }
                                                    cover
                                                    className="w-full h-full object-center object-cover"
                                                    data-anim="fade"
                                                />
                                            </Zoom>
                                        </div>
                                    )}
                                </InView>
                            </div>
                            {/*décoration*/}
                            <div className="bg-amber-100 w-full h-48 absolute bottom-0 left-full z-0"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Overview
