import ButtonLink from "@/components/elements/button-link"
import { InView } from "react-intersection-observer"
import classNames from "classnames"
import Markdown from "react-markdown"

const CallToActions = ({ data }) => {
    return (
        <section className="bg-primary-100 py-20 text-center">
            <div className="text-center">
                {data.title && (
                    <InView threshold="1" triggerOnce="true">
                        {({ inView, ref }) => (
                            <h1
                                ref={ref}
                                className={classNames(
                                    "text-xl font-extrabold tracking-tight  text-gray-800 sm:text-2xl md:text-3xl",
                                    inView ? "is-in-view" : "is-out-view"
                                )}
                            >
                                {data.title}
                            </h1>
                        )}
                    </InView>
                )}
                {data.description && (
                    <InView threshold="1" triggerOnce="true">
                        {({ inView, ref }) => (
                            <p
                                ref={ref}
                                className={classNames(
                                    "text-gray-700 mt-3 max-w-md mx-auto text-base sm:text-lg md:mt-5 md:text-xl md:max-w-3xl",
                                    inView ? "is-in-view" : "is-out-view"
                                )}
                            >
                                {data.description}
                            </p>
                        )}
                    </InView>
                )}
                {data.buttons && (
                    <div className="mt-5 mx-auto flex flex-col sm:flex-row sm:flex sm:justify-center gap-4 gap-y-2">
                        {data.buttons.map((button, index) => (
                            <ButtonLink
                                button={button}
                                theme={button.type}
                                key={index}
                            />
                        ))}
                    </div>
                )}
                {data.smallTextWithLink && (
                    <div className="text-base md:text-sm mt-4 sm:mt-3 rich-text-hero">
                        <Markdown>{data.smallTextWithLink}</Markdown>
                    </div>
                )}
            </div>
        </section>
    )
}

export default CallToActions
