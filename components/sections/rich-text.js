import Markdown from "react-markdown"

const RichText = ({ data }) => {
    return (
        <section className="prose prose-lg container py-12">
            <Markdown>{data.content}</Markdown>
        </section>
    )
}

export default RichText
