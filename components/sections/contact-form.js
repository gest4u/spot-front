import { useState } from "react"
import { fetchAPI } from "utils/api"
import * as yup from "yup"
import { Formik, Form, Field } from "formik"
import Button from "../elements/button"

const ContactForm = ({ data }) => {
    const [loading, setLoading] = useState(false)

    const LeadSchema = yup.object().shape({
        email: yup.string().email().required(),
    })

    return (
        <section className="py-10 bg-amber-50">
            <div className="container">
                <div className="px-6 py-6">
                    <Formik
                        initialValues={{ email: "" }}
                        validationSchema={LeadSchema}
                        onSubmit={async (
                            values,
                            { setSubmitting, setErrors }
                        ) => {
                            setLoading(true)
                            try {
                                setErrors({ api: null })
                                await fetchAPI("/contact-form-submissions", {
                                    method: "POST",
                                    body: JSON.stringify({
                                        firstName: values.firstName,
                                        lastName: values.lastName,
                                        email: values.email,
                                        textArea: values.textArea,
                                        phone: values.textArea,
                                    }),
                                })
                            } catch (err) {
                                setErrors({ api: err.message })
                            }

                            setLoading(false)
                            setSubmitting(false)
                        }}
                    >
                        {({ errors, touched, isSubmitting }) => (
                            <div className="max-w-xl m-auto mt-8">
                                <Form className="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-12">
                                    <div className="sm:col-span-6">
                                        <label
                                            htmlFor="firstName"
                                            className="block text-sm font-medium text-gray-700"
                                        >
                                            Nom
                                        </label>
                                        <Field
                                            className="w-full border-gray-500 px-5 py-3 placeholder-gray-500 focus:outline-none  focus:border-gray-700 rounded-md"
                                            type="text"
                                            name="firstName"
                                            placeholder={
                                                data.firstNamePlaceholder
                                            }
                                        />
                                    </div>
                                    <div className="sm:col-span-6">
                                        <label
                                            htmlFor="lastName"
                                            className="block text-sm font-medium text-gray-700"
                                        >
                                            Prénom
                                        </label>
                                        <Field
                                            className="w-full border-gray-500 px-5 py-3 placeholder-gray-500 focus:outline-none  focus:border-gray-700 rounded-md"
                                            type="text"
                                            name="lastName"
                                            placeholder={
                                                data.lastNamePlaceholder
                                            }
                                        />
                                    </div>
                                    <div className="sm:col-span-12">
                                        <label
                                            htmlFor="email"
                                            className="block text-sm font-medium text-gray-700"
                                        >
                                            Adresse email
                                        </label>
                                        <Field
                                            className="w-full border-gray-500 px-5 py-3 placeholder-gray-500 focus:outline-none  focus:border-gray-700 rounded-md"
                                            type="email"
                                            name="email"
                                            placeholder={data.emailPlaceholder}
                                        />
                                    </div>
                                    <div className="sm:col-span-12">
                                        <label
                                            htmlFor="textArea"
                                            className="block text-sm font-medium text-gray-700"
                                        >
                                            Message
                                        </label>
                                        <Field
                                            className="w-full border-gray-500 px-5 py-3 placeholder-gray-500 focus:outline-none  focus:border-gray-700 rounded-md"
                                            as="textarea"
                                            name="textArea"
                                            placeholder={
                                                data.textAreaPlaceholder
                                            }
                                        />
                                    </div>
                                    <div className="sm:col-span-12">
                                        <label
                                            htmlFor="textArea"
                                            className="block text-sm font-medium text-gray-700"
                                        >
                                            Téléphone
                                        </label>
                                        <Field
                                            className="w-full border-gray-500 px-5 py-3 placeholder-gray-500 focus:outline-none  focus:border-gray-700 rounded-md"
                                            as="textarea"
                                            name="phone"
                                            placeholder={"téléphone"}
                                        />
                                    </div>
                                    <div className="sm:col-span-12">
                                        <Button
                                            type="submit"
                                            button={data.submitButton}
                                            key={data.submitButton.id}
                                            disabled={isSubmitting}
                                            loading={loading}
                                            theme={data.submitButton.type}
                                        />
                                    </div>
                                </Form>
                                <p className="text-red-500 h-12 text-sm mt-1 ml-2 text-left">
                                    {(errors.email &&
                                        touched.email &&
                                        errors.email) ||
                                        errors.api}
                                </p>
                            </div>
                        )}
                    </Formik>
                </div>
            </div>
        </section>
    )
}

export default ContactForm
