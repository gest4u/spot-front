import NextImage from "../elements/image"
import classNames from "classnames"
import { InView } from "react-intersection-observer"

const FeatureColumnsGroup = ({ data }) => {
    return (
        <section className="container flex flex-col gap-12 py-12">
            <div className="relative lg:pl-5 text-center">
                {data.label && (
                    <InView threshold="1" triggerOnce="true">
                        {({ inView, ref }) => (
                            <div
                                ref={ref}
                                className={classNames(
                                    "line-wrapper relative mx-auto",
                                    inView ? "is-in-view" : "is-out-view"
                                )}
                            >
                                <span className="relative line-element sm:text-lg font-semibold mb-3">
                                    {data.label}
                                </span>
                            </div>
                        )}
                    </InView>
                )}
                {data.title && (
                    <InView threshold="1" triggerOnce="true">
                        {({ inView, ref }) => (
                            <h1
                                ref={ref}
                                className={classNames(
                                    "text-3xl font-extrabold tracking-tight text-gray-800 sm:text-5xl md:text-5xl mx-auto max-w-md md:max-w-3xl",
                                    inView ? "is-in-view" : "is-out-view"
                                )}
                            >
                                {data.title}
                            </h1>
                        )}
                    </InView>
                )}
                {data.description && (
                    <InView threshold="1" triggerOnce="true">
                        {({ inView, ref }) => (
                            <p
                                ref={ref}
                                className={classNames(
                                    "text-gray-500 mt-3 md:mt-5 text-base sm:text-lg md:text-xl mx-auto max-w-md md:max-w-3xl",
                                    inView ? "is-in-view" : "is-out-view"
                                )}
                            >
                                {data.description}
                            </p>
                        )}
                    </InView>
                )}
            </div>
            <div className="relative lg:grid lg:grid-cols-4 lg:items-start  gap-8 align-top py-12 px-5">
                {data.features.map((feature, index) => (
                    <InView threshold="1" triggerOnce="true" key={index}>
                        {({ inView, ref }) => (
                            <div
                                ref={ref}
                                className={classNames(
                                    "text-center flex-1 text-lg bg-gray-50 p-4 border border-gray-100",
                                    inView ? "is-in-view" : "is-out-view"
                                )}
                            >
                                <div className=" mx-auto  flex items-center justify-center h-12 w-12 rounded-md bg-amber-100 dark">
                                    <div className="mx-auto w-10 h-10">
                                        <NextImage media={feature.icon} />
                                    </div>
                                </div>
                                <h3 className="font-bold mt-4 mb-4">
                                    {feature.title}
                                </h3>
                                <p className="text-gray-500">
                                    {feature.description}
                                </p>
                            </div>
                        )}
                    </InView>
                ))}
            </div>
        </section>
    )
}

export default FeatureColumnsGroup
