import { useState } from "react"
import NextImage from "@/components/elements/image"

import SwiperCore, { Autoplay, Navigation, Pagination, Thumbs } from "swiper"
import { Swiper, SwiperSlide } from "swiper/react"

SwiperCore.use([Autoplay, Navigation, Pagination, Thumbs])

import { InView } from "react-intersection-observer"
import classNames from "classnames"
import ButtonLink from "@/components/elements/button-link"

const HeroSlider = ({ data }) => {
    const interleaveOffset = 0.5
    const speed = 1300

    const onProgress = (swiper) => {
        for (var i = 0; i < swiper.slides.length; i++) {
            var slideProgress = swiper.slides[i].progress

            var innerOffset = swiper.width * interleaveOffset
            var innerTranslate = slideProgress * innerOffset
            swiper.slides[i].querySelector(
                ".js-parallax-slide-bg"
            ).style.transform = "translate3d(" + innerTranslate + "px, 0, 0)"
        }
    }
    const onSetTransition = (swiper) => {
        for (var i = 0; i < swiper.slides.length; i++) {
            swiper.slides[i].style.transition = ""
            swiper.slides[i].querySelector(
                ".js-parallax-slide-bg"
            ).style.transition = speed + "ms"
        }
    }
    const onTouchStart = (swiper) => {
        for (var i = 0; i < swiper.slides.length; i++) {
            swiper.slides[i].style.transition = ""
        }
    }
    const [thumbsSwiper, setThumbsSwiper] = useState(null)

    return (
        <section className="heroSlider relative bg-ember-100">
            {/* Background image and overlap */}
            <div aria-hidden="true" className="absolute inset-0 flex flex-col">
                <div className="flex-1 relative w-full ">
                    <div className="absolute inset-0 w-full">
                        <div className="relative h-full w-full">
                            <Swiper
                                thumbs={{ swiper: thumbsSwiper }}
                                className="mySwiperFull h-full w-full relative overflow-hidden "
                                speed={1300}
                                watchSlidesProgress={true}
                                autoplay={{ delay: 6000 }}
                                navigation={{
                                    nextEl: ".swiper-button-next",
                                    prevEl: ".swiper-button-prev",
                                }}
                                pagination={{
                                    clickable: true,
                                    el: ".custom-swiper-pagination",
                                }}
                                spaceBetween={0}
                                slidesPerView={1}
                                onProgress={(swiper) => onProgress(swiper)}
                                onSetTransition={(swiper) =>
                                    onSetTransition(swiper)
                                }
                                onTouchStart={(swiper) => onTouchStart(swiper)}
                            >
                                {data.slides.map((slide, index) => (
                                    <SwiperSlide
                                        key={index}
                                        className="relative overflow-hidden h-full overflow-hidden"
                                    >
                                        <div className="js-parallax-slide-bg h-full w-full absolute top-0 ">
                                            <NextImage
                                                media={slide.media}
                                                cover
                                                className="relative w-full h-full overflow-hidden"
                                            />
                                        </div>
                                    </SwiperSlide>
                                ))}
                            </Swiper>
                        </div>
                    </div>
                    <div className="absolute inset-0 bg-gradient-to-t from-black via-gray-800 to-black opacity-60" />
                </div>
                <div className="w-full bg-gradient-to-b from-gray-300 to-white h-32 md:h-40 lg:h-48" />
            </div>
            <div className="relative max-w-3xl mx-auto px-4 text-center sm:pb-0 sm:px-6 lg:px-8">
                <div className="relative py-36">
                    {data.title && (
                        <InView threshold="1" triggerOnce="true">
                            {({ inView, ref }) => (
                                <h1
                                    ref={ref}
                                    className={classNames(
                                        "text-4xl font-extrabold tracking-tight text-white sm:text-5xl md:text-6xl",
                                        inView ? "is-in-view" : "is-out-view"
                                    )}
                                >
                                    {data.title}
                                </h1>
                            )}
                        </InView>
                    )}
                    {data.description && (
                        <InView threshold="1" triggerOnce="true">
                            {({ inView, ref }) => (
                                <p
                                    ref={ref}
                                    className={classNames(
                                        "text-gray-100 mt-3 max-w-md mx-auto text-base sm:text-lg md:mt-5 md:text-xl md:max-w-3xl",
                                        inView ? "is-in-view" : "is-out-view"
                                    )}
                                >
                                    {data.description}
                                </p>
                            )}
                        </InView>
                    )}
                    {data.buttons && (
                        <div className="mt-5 mx-auto flex flex-col sm:flex-row sm:flex sm:justify-center gap-4 gap-y-2">
                            {data.buttons.map((button, index) => (
                                <ButtonLink
                                    button={button}
                                    theme={button.type}
                                    key={index}
                                />
                            ))}
                        </div>
                    )}
                </div>
            </div>

            <div
                aria-labelledby="collection-heading"
                className="relative sm:mt-0 container"
            >
                <h2 id="collection-heading text-gray-100" className="sr-only">
                    En photo
                </h2>
                <Swiper
                    onSwiper={setThumbsSwiper}
                    slidesPerView={2}
                    breakpoints={{ 1024: { slidesPerView: 3 } }}
                    loop={false}
                    spaceBetween={50}
                    freeMode={true}
                    watchSlidesVisibility={true}
                    watchSlidesProgress={true}
                    className="mySwiperThumbs w-full"
                >
                    {data.slides.map((slide, index) => (
                        <SwiperSlide key={index}>
                            <div
                                aria-hidden="true"
                                className="img-container relative aspect-w-16 aspect-h-10 overflow-hidden"
                            >
                                <NextImage
                                    media={slide.media}
                                    cover
                                    className="w-full h-full object-center object-cover"
                                />
                                <div className="absolute inset-0 bg-gradient-to-b from-transparent to-white opacity-20"></div>
                            </div>
                            {/*<div className="text-black flex items-center">
                                <div className="dot my-2"></div>
                                <span className="my-2 title text-xs">
                                    {slide.title}
                                </span>
                            </div>*/}
                        </SwiperSlide>
                    ))}
                </Swiper>
            </div>
        </section>
    )
}

export default HeroSlider
