import { useState } from "react"
import { fetchAPI } from "utils/api"
import * as yup from "yup"
import { Formik, Form, Field } from "formik"
import Button from "../elements/button"

const NewsletterForm = ({ data }) => {
    const [loading, setLoading] = useState(false)

    const LeadSchema = yup.object().shape({
        email: yup.string().email().required(),
    })

    return (
        <section className="py-10 bg-gray-50 border-t border-gray-100">
            <div className="container">
                <div className="px-6 py-6 bg-gray-800 md:py-12 md:px-12 lg:py-16 lg:px-16 xl:flex xl:items-center">
                    <div className="xl:w-0 xl:flex-1 text-center xl:text-left">
                        <h2 className="text-2xl font-extrabold tracking-tight text-white sm:text-3xl">
                            {data.title}
                        </h2>
                        <p className="mt-3 max-w-3xl text-lg leading-6 text-emerald-50">
                            {data.description}
                        </p>
                    </div>

                    <Formik
                        initialValues={{ email: "" }}
                        validationSchema={LeadSchema}
                        onSubmit={async (
                            values,
                            { setSubmitting, setErrors }
                        ) => {
                            setLoading(true)

                            try {
                                setErrors({ api: null })
                                await fetchAPI("/lead-form-submissions", {
                                    method: "POST",
                                    body: JSON.stringify({
                                        email: values.email,
                                        location: data.location,
                                    }),
                                })
                            } catch (err) {
                                setErrors({ api: err.message })
                            }

                            setLoading(false)
                            setSubmitting(false)
                        }}
                    >
                        {({ errors, touched, isSubmitting }) => (
                            <div className="mt-8 sm:w-full xl:max-w-md xl:max-w-md xl:mt-0 xl:ml-8">
                                <Form className="flex flex-col md:flex-row gap-4">
                                    <label htmlFor="email" className="sr-only">
                                        Email address
                                    </label>
                                    <Field
                                        className="w-full border-white px-5 py-3 placeholder-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-indigo-700 focus:ring-white rounded-md"
                                        type="email"
                                        name="email"
                                        placeholder={data.emailPlaceholder}
                                    />
                                    <Button
                                        type="submit"
                                        button={data.submitButton}
                                        key={data.submitButton.id}
                                        disabled={isSubmitting}
                                        loading={loading}
                                        theme={data.submitButton.type}
                                    />
                                </Form>
                                <p className="text-red-500 h-12 text-sm mt-1 ml-2 text-left">
                                    {(errors.email &&
                                        touched.email &&
                                        errors.email) ||
                                        errors.api}
                                </p>
                            </div>
                        )}
                    </Formik>
                </div>
            </div>
        </section>
    )
}

export default NewsletterForm
