import Link from "next/link"

const Pricing = ({ data }) => {
    return (
        <section className="container">
            <div className="">
                <div className="sm:py-6 lg:pb-8 px-4 sm:px-6 lg:px-8">
                    <div className="">
                        <div className="text-center mb-2">
                            <h2 className="m-auto max-w-xl text-3xl font-extrabold tracking-tight text-gray-800 sm:text-4xl md:text-5xl">
                                {data.name}
                            </h2>
                            <p className="text-gray-500 my-3 max-w-md mx-auto text-base sm:text-lg md:mt-5 md:text-xl md:max-w-3xl">
                                {data.description}
                            </p>
                        </div>
                        <div className="bg-amber-50 p-4 mt-8">
                            <div className="m-auto divide-y divide-gray-800">
                                {data.plans.map((plan, index) => (
                                    <div
                                        className="flex items-center max-w-5xl m-auto"
                                        key={index}
                                    >
                                        <h2 className="font-normal w-1/3">
                                            {plan.title}
                                        </h2>
                                        <ul className="ml-4 flex flex-1">
                                            {plan.prices.map((price, index) => (
                                                <li
                                                    key={index}
                                                    className="flex-1 py-2 "
                                                >
                                                    <div
                                                        className={
                                                            "block text-xl font-extrabold"
                                                        }
                                                    >
                                                        {price.amount} € TTC
                                                    </div>
                                                    <div className={"text-sm"}>
                                                        {price.legend}
                                                    </div>
                                                </li>
                                            ))}
                                        </ul>
                                    </div>
                                ))}
                            </div>
                            <div className="flex justify-center">
                                <Link href="/reservation">
                                    <a className="my-8 text-center md:text-lg rounded-md md:py-2 md:px-6 btn-secondary">
                                        Contactez-nous pour réserver
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/*old*/}
        </section>
    )
}

export default Pricing
