import Navbar from "./elements/navbar"
import Footer from "./elements/footer"

const Layout = ({ children, global, navigationTheme }) => {
    const { navbar, footer, phone } = global
    return (
        <div className="flex flex-col justify-between min-h-screen">
            {/* Aligned to the top */}
            <div className="flex-1 relative">
                <div className="absolute top-0 w-full z-10">
                    <Navbar
                        navbar={navbar}
                        phone={phone}
                        navigationTheme={navigationTheme}
                    />
                </div>
                {/* page or post content  is here */}
                {children}
                {/* page or post content  is here */}
            </div>
            {/* Aligned to the bottom */}
            <Footer footer={footer} />
        </div>
    )
}

export default Layout
