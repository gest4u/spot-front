import classNames from "classnames"
import CustomLink from "./custom-link"

const ButtonLink = ({ button, theme, compact = false }) => {
    return (
        <CustomLink link={button}>
            <ButtonContent button={button} theme={theme} compact={compact} />
        </CustomLink>
    )
}

const ButtonContent = ({ button, theme, compact }) => {
    return (
        <div
            className={classNames(
                // Common classes
                "flex w-full justify-center items-center lg:w-auto text-center md:text-lg rounded-md",
                // Full-size button
                {
                    "md:py-4 md:px-10": compact === false,
                },
                // Compact button
                {
                    "md:py-2 md:px-6": compact === true,
                },
                // Specific to when the button is  primary
                {
                    "btn-primary": theme === "primary",
                },
                // Specific to when the button is  secondary
                {
                    "btn-secondary": theme === "secondary",
                },
                // Specific to when the button is  white
                {
                    "btn-white": theme === "white",
                },
                // Specific to when the button is  black
                {
                    "btn-black": theme === "black",
                }
            )}
        >
            {button.text}
        </div>
    )
}

export default ButtonLink
