import { NextSeo } from "next-seo"
import { getStrapiMedia } from "utils/media"

const Seo = ({ metadata }) => {
    // Prevent errors if no metadata was set
    if (!metadata) return null

    return (
        <NextSeo
            title={metadata.metaTitle}
            description={metadata.metaDescription}
            openGraph={{
                type: "website",
                locale: "fr_FR",
                url: `${process.env.SITE_URL || "http://localhost:3000"}`,
                site_name: `${process.env.SITE_NAME || "Nom du site"}`,
                // Title and description are mandatory
                title: metadata.metaTitle,
                description: metadata.metaDescription,
                // Only include OG image if we have it
                // Careful: if you disable image optimization in Strapi, this will break
                ...(metadata.shareImage && {
                    images: Object.values(metadata.shareImage.formats).map(
                        (image) => {
                            return {
                                url: getStrapiMedia(image.url),
                                width: image.width,
                                height: image.height,
                            }
                        }
                    ),
                }),
            }}
            // Only included Twitter data if we have it
            twitter={{
                ...(metadata.twitterCardType && {
                    cardType: metadata.twitterCardType,
                }),
                // Handle is the twitter username of the content creator
                ...(metadata.twitterUsername && {
                    handle: metadata.twitterUsername,
                }),
            }}
        />
    )
}

export default Seo
