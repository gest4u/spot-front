import { Fragment } from "react"

import { Popover, Transition } from "@headlessui/react"
import { MenuIcon, XIcon } from "@heroicons/react/outline"

import Link from "next/link"
import { useRouter } from "next/router"

import ButtonLink from "./button-link"
import NextImage from "./image"
import CustomLink from "./custom-link"

import classNames from "classnames"

const Navbar = ({ navbar, navigationTheme, phone }) => {
    const router = useRouter()
    return (
        <Popover>
            {({ open }) => (
                <Fragment>
                    {/* The desktop navbar */}
                    <nav className="px-4 sm:px-6">
                        <div className="container flex items-center justify-between md:justify-center  py-10">
                            <div className="relative w-full flex items-center justify-between md:justify-center">
                                <div className="flex items-center justify-between w-full md:w-auto flex-1 md:absolute md:inset-y-0 md:left-0">
                                    <Link href="/">
                                        <a className="relative flex items-center">
                                            <span className="sr-only">
                                                Le spot
                                            </span>
                                            {navigationTheme ==
                                                "transparent" && (
                                                <NextImage
                                                    width="180"
                                                    height="50"
                                                    media={navbar.logoLight}
                                                />
                                            )}
                                            {navigationTheme == "light" && (
                                                <NextImage
                                                    width="180"
                                                    height="50"
                                                    media={navbar.logoLight}
                                                />
                                            )}
                                            {navigationTheme == "dark" && (
                                                <NextImage
                                                    width="180"
                                                    height="50"
                                                    media={navbar.logoDark}
                                                />
                                            )}
                                            {/* Article du blog*/}
                                            {navigationTheme == null && (
                                                <NextImage
                                                    width="180"
                                                    height="50"
                                                    media={navbar.logoDark}
                                                />
                                            )}
                                        </a>
                                    </Link>
                                    <div className="-mr-2 flex items-center md:hidden">
                                        <Popover.Button
                                            className={classNames(
                                                "rounded-md p-2 inline-flex items-center justify-center focus:outline-none focus:ring-2 focus:ring-inset ",
                                                {
                                                    "bg-gray-50 text-gray-400 hover:text-gray-500 hover:bg-gray-100":
                                                        navigationTheme ===
                                                        "transparent",
                                                },
                                                {
                                                    "bg-gray-50 text-gray-400 hover:text-gray-500 hover:bg-gray-100":
                                                        navigationTheme ===
                                                        "light",
                                                },
                                                {
                                                    "bg-gray-800 text-gray-50 hover:text-gray-100 hover:bg-gray-700":
                                                        navigationTheme ===
                                                        "dark",
                                                }
                                            )}
                                        >
                                            <span className="sr-only">
                                                Open mobile navbar
                                            </span>
                                            <MenuIcon
                                                className="h-6 w-6"
                                                aria-hidden="true"
                                            />
                                        </Popover.Button>
                                    </div>
                                </div>
                                <div className="hidden md:flex md:space-x-10">
                                    {navbar.links.map((navLink) => (
                                        <div key={navLink.id}>
                                            <CustomLink
                                                link={navLink}
                                                locale={router.locale}
                                            >
                                                <div
                                                    className={classNames(
                                                        "font-medium",
                                                        {
                                                            "text-gray-200 hover:text-gray-100":
                                                                navigationTheme ===
                                                                "transparent",
                                                        },
                                                        {
                                                            "text-gray-200 hover:text-gray-100":
                                                                navigationTheme ===
                                                                "light",
                                                        },
                                                        {
                                                            "text-gray-800 hover:text-gray-900":
                                                                navigationTheme ===
                                                                "dark",
                                                        }
                                                    )}
                                                >
                                                    {navLink.text}
                                                </div>
                                            </CustomLink>
                                        </div>
                                    ))}
                                </div>
                                <div className="hidden md:absolute md:flex md:items-center md:justify-end md:inset-y-0 md:right-0">
                                    <div className="hidden lg:block px-5 py-3">
                                        <a
                                            href={`tel:${phone}`}
                                            className={classNames(
                                                "-m-3 p-3 block text-sm rounded-md  transition ease-in-out duration-150",
                                                {
                                                    "text-gray-200 hover:text-gray-100":
                                                        navigationTheme ===
                                                        "transparent",
                                                },
                                                {
                                                    "text-gray-200 hover:text-gray-100":
                                                        navigationTheme ===
                                                        "light",
                                                },
                                                {
                                                    "text-gray-800 hover:text-gray-900":
                                                        navigationTheme ===
                                                        "dark",
                                                }
                                            )}
                                        >
                                            <span className="mt-1 leading-none">
                                                <span className="text-xs">
                                                    Appelez-nous!
                                                </span>
                                                <br />
                                                <span className="font-bold">
                                                    {phone}
                                                </span>
                                            </span>
                                        </a>
                                    </div>
                                    {navbar.button && (
                                        <div className="hidden md:block gap-4">
                                            <ButtonLink
                                                button={navbar.button}
                                                theme={navbar.button.type}
                                                compact
                                            />
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                    </nav>
                    {/* Show the mobile navbar */}
                    <Transition
                        show={open}
                        as={Fragment}
                        enter="duration-150 ease-out"
                        enterFrom="opacity-0 scale-95"
                        enterTo="opacity-100 scale-100"
                        leave="duration-100 ease-in"
                        leaveFrom="opacity-100 scale-100"
                        leaveTo="opacity-0 scale-95"
                    >
                        <Popover.Panel
                            focus
                            static
                            className="absolute top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden"
                        >
                            <div className="rounded-lg shadow-md bg-white ring-1 ring-black ring-opacity-5 overflow-hidden">
                                <div className="px-5 pt-4 flex items-center justify-between">
                                    <div className="-mr-2">
                                        <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                                            <span className="sr-only">
                                                Close menu
                                            </span>
                                            <XIcon
                                                className="h-6 w-6"
                                                aria-hidden="true"
                                            />
                                        </Popover.Button>
                                    </div>
                                </div>
                                <div className="px-2 pt-2 pb-3">
                                    <div className="flex items-center justify-between w-full md:w-auto flex-1 md:absolute md:inset-y-0 md:left-0">
                                        <Link href="/">
                                            <a className="relative flex items-center">
                                                <span className="sr-only">
                                                    Le spot
                                                </span>
                                                <NextImage
                                                    width="120"
                                                    height="33"
                                                    media={navbar.logo}
                                                />
                                            </a>
                                        </Link>
                                    </div>
                                    <div className="hidden md:flex md:space-x-10">
                                        {navbar.links.map((navLink) => (
                                            <div key={navLink.id}>
                                                <CustomLink
                                                    link={navLink}
                                                    locale={router.locale}
                                                >
                                                    <div
                                                        className={classNames(
                                                            "font-medium",
                                                            {
                                                                "text-gray-200 hover:text-gray-100":
                                                                    navigationTheme ===
                                                                    "transparent",
                                                            },
                                                            {
                                                                "text-gray-200 hover:text-gray-100":
                                                                    navigationTheme ===
                                                                    "light",
                                                            },
                                                            {
                                                                "text-gray-800 hover:text-gray-900":
                                                                    navigationTheme ===
                                                                    "dark",
                                                            }
                                                        )}
                                                    >
                                                        {navLink.text}
                                                    </div>
                                                </CustomLink>
                                            </div>
                                        ))}
                                    </div>
                                    <div className="px-5 py-3">
                                        <a
                                            href={`tel:${phone}`}
                                            className={classNames(
                                                "-m-3 p-3 block text-sm rounded-md  transition ease-in-out duration-150",
                                                {
                                                    "text-gray-200 hover:text-gray-100":
                                                        navigationTheme ===
                                                        "transparent",
                                                },
                                                {
                                                    "text-gray-200 hover:text-gray-100":
                                                        navigationTheme ===
                                                        "light",
                                                },
                                                {
                                                    "text-gray-800 hover:text-gray-900":
                                                        navigationTheme ===
                                                        "dark",
                                                }
                                            )}
                                        >
                                            <span className="mt-1 leading-none">
                                                <span className="text-xs">
                                                    Appelez-nous!
                                                </span>
                                                <br />
                                                <span className="font-bold">
                                                    {phone}
                                                </span>
                                            </span>
                                        </a>
                                    </div>

                                    {navbar.button && (
                                        <div className="block">
                                            <ButtonLink
                                                button={navbar.button}
                                                theme={navbar.button.type}
                                                compact
                                            />
                                        </div>
                                    )}
                                </div>
                            </div>
                        </Popover.Panel>
                    </Transition>
                </Fragment>
            )}
        </Popover>
    )
}

export default Navbar
