import classNames from "classnames"
import Loader from "./loader"

const Button = ({ button, compact = false, handleClick, loading = false }) => {
    return (
        <button link={button} onClick={handleClick}>
            <div
                className={classNames(
                    // Common classes
                    "flex w-full justify-center items-center lg:w-auto text-center md:text-lg rounded-md",
                    // Full-size button
                    {
                        "md:py-4 md:px-10": compact === false,
                    },
                    // Compact button
                    {
                        "md:py-2 md:px-6": compact === true,
                    },
                    // Specific to when the button is  primary
                    {
                        "btn-primary": button.type === "primary",
                    },
                    // Specific to when the button is  secondary
                    {
                        "btn-secondary": button.type === "secondary",
                    },
                    // Specific to when the button is  white
                    {
                        "btn-white": button.type === "white",
                    },
                    // Specific to when the button is  black
                    {
                        "btn-black": button.type === "dark",
                    }
                )}
            >
                {loading && <Loader />}
                {button.text}
            </div>
        </button>
    )
}
export default Button
