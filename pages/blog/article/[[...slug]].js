import ErrorPage from "next/error"
import { getPostData, fetchAPI, getGlobalData } from "utils/api"
import Sections from "@/components/postBuilder"
import Seo from "@/components/elements/seo"
import { useRouter } from "next/router"
import Layout from "@/components/layout"
import { getLocalizedPostPaths } from "utils/localize"

// The file is called [[...slug]].js because we're using Next's
// optional catch all routes feature. See the related docs:
// https://nextjs.org/docs/routing/dynamic-routes#optional-catch-all-routes

const DynamicPost = ({
    global,
    postAbstractData,
    metadata,
    sections,
    preview,
    viewContext,
}) => {
    const router = useRouter()

    // Check if the required data was provided
    if (!router.isFallback && !sections?.length) {
        return <ErrorPage statusCode={404} />
    }

    // Loading screen (only possible in preview mode)
    if (router.isFallback) {
        return <div className="container">Loading...</div>
    }
    // Build  page render
    return (
        <Layout global={global} viewContext={viewContext}>
            {/* Add meta tags for SEO*/}
            <Seo metadata={metadata} />
            {/* Display content sections */}
            <Sections
                sections={sections}
                preview={preview}
                postAbstractData={postAbstractData}
            ></Sections>
        </Layout>
    )
}

export async function getStaticPaths(context) {
    // Get all posts from Strapi
    const allPosts = context.locales.map(async (locale) => {
        const localePosts = await fetchAPI(`/posts?_locale=${locale}`)
        return localePosts
    })
    const posts = allPosts.flat()
    const paths = posts.map((post) => {
        // Decompose the slug that was saved in Strapi
        const slugArray = !post.slug ? false : post.slug.split("/")
        return {
            params: { slug: slugArray },
            // Specify the locale to render
            locale: post.locale,
        }
    })

    return { paths, fallback: true }
}

export async function getStaticProps(context) {
    const { params, locale, locales, defaultLocale, preview = null } = context
    const globalLocale = await getGlobalData(locale)
    // Fetch post. Include drafts if preview mode is on
    const postData = await getPostData(
        { slug: !params.slug ? [""] : params.slug },
        locale,
        preview
    )
    if (postData == null) {
        // Giving the post no props will trigger a 404 post
        return { props: {} }
    }

    // We have the required post data, pass it to the post component
    const { contentSections, metadata, localizations, slug } = postData
    const viewContext = {
        slug,
        locale: postData.locale,
        locales,
        defaultLocale,
        localizations,
    }
    const postAbstractData = {
        shortName: postData.shortName,
        excerpt: postData.excerpt,
    }
    //Path for blog/article....
    const localizedPaths = getLocalizedPostPaths(viewContext)

    return {
        props: {
            preview,
            sections: contentSections,
            metadata,
            postAbstractData: {
                ...postAbstractData,
            },
            global: globalLocale,
            viewContext: {
                ...viewContext,
                localizedPaths,
            },
        },
    }
}

export default DynamicPost
